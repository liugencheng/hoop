package com.hoop.web.controller.monitor;

import com.hoop.common.core.domain.AjaxResult;

import com.hoop.web.domain.Server;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控（第三方）
 *
 * @author dongua
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController {
    @PreAuthorize("@ss.hasPermi('monitor:server:list')")
    @GetMapping()
    public AjaxResult getInfo() throws Exception {
        // 服务器相关信息封装
        Server server = new Server();
        // 获取、设置服务器系统信息
        server.copyTo();
        return AjaxResult.success(server);
    }
}
