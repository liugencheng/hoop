package com.hoop.web.controller.monitor;

import com.hoop.common.annotation.Log;
import com.hoop.common.constant.Constants;
import com.hoop.common.core.controller.BaseController;
import com.hoop.common.core.domain.AjaxResult;
import com.hoop.common.core.domain.model.LoginUser;
import com.hoop.common.core.page.TableDataInfo;
import com.hoop.common.core.redis.RedisCache;
import com.hoop.common.enums.BusinessType;
import com.hoop.common.utils.StringUtils;

import com.hoop.domain.SysUserOnline;
import com.hoop.service.ISysUserOnlineService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 在线用户监控
 *
 * @author dongua
 */
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {
    @Resource
    private ISysUserOnlineService sysUserOnlineService;

    @Resource
    private RedisCache redisCache;

    @PreAuthorize("@ss.hasPermi('monitor:online:list')")
    @GetMapping("/list")
    public TableDataInfo list(String ipaddr, String userName) {
        // 获取在 redis 中所有的用户
        Collection<String> keys = redisCache.keys(Constants.LOGIN_TOKEN_KEY + "*");
        List<SysUserOnline> userOnlineList = new ArrayList<>();
        for (String key : keys) {
            LoginUser user = redisCache.getCacheObject(key);
            // 判断查询条件
            if (StringUtils.isNotEmpty(ipaddr) && StringUtils.isNotEmpty(userName)) {
                if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByInfo(ipaddr, userName, user));
                }
            } else if (StringUtils.isNotEmpty(ipaddr)) {
                if (StringUtils.equals(ipaddr, user.getIpaddr())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByIpaddr(ipaddr, user));
                }
            } else if (StringUtils.isNotEmpty(userName) && StringUtils.isNotNull(user.getUser())) {
                if (StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(sysUserOnlineService.selectOnlineByUserName(userName, user));
                }
            } else {
                // 封装成 SysUserOnline 对象集合
                userOnlineList.add(sysUserOnlineService.loginUserToUserOnline(user));
            }
        }
        // 反转列表元素顺序
        Collections.reverse(userOnlineList);
        return getDataTable(userOnlineList);
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    public AjaxResult forceLogout(@PathVariable String tokenId) {
        redisCache.deleteObject(Constants.LOGIN_TOKEN_KEY + tokenId);
        return AjaxResult.success();
    }
}
