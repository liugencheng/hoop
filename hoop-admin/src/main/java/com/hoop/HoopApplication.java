package com.hoop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author dongua
 * @date 2021/12/15 19:42
 **/
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableCircuitBreaker
@EnableFeignClients
@EnableEurekaClient
@EnableHystrix
@EnableDiscoveryClient
@EnableHystrixDashboard
public class HoopApplication {

    public static void main(String[] args) {
        SpringApplication.run(HoopApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  紧箍启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
