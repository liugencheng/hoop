package com.hoop.service;

import com.hoop.common.core.domain.model.LoginUser;
import com.hoop.config.HystrixCon;
import com.hoop.domain.SysUserOnline;
import com.hoop.service.impl.ISysUserOnlineServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 在线用户 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysUserOnline",
        configuration = HystrixCon.class, fallback = ISysUserOnlineServiceImpl.class)
public interface ISysUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user   用户信息
     * @return 在线用户信息
     */
    @RequestMapping(value = "/selectOnlineByIpaddr", method = RequestMethod.GET)
    SysUserOnline selectOnlineByIpaddr(@RequestParam("ipaddr") String ipaddr, @RequestBody LoginUser user);

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @RequestMapping(value = "/selectOnlineByUserName", method = RequestMethod.GET)
    SysUserOnline selectOnlineByUserName(@RequestParam("userName") String userName,@RequestBody LoginUser user);

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr   登录地址
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @RequestMapping(value = "/selectOnlineByInfo", method = RequestMethod.GET)
    SysUserOnline selectOnlineByInfo(@RequestParam("ipaddr") String ipaddr,@RequestParam("userName") String userName,@RequestBody LoginUser user);

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    @RequestMapping(value = "/loginUserToUserOnline", method = RequestMethod.POST)
    SysUserOnline loginUserToUserOnline(@RequestBody LoginUser user);
}
