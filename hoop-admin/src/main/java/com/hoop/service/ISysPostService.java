package com.hoop.service;

import com.hoop.config.HystrixCon;
import com.hoop.domain.SysPost;
import com.hoop.service.impl.ISysPostServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 岗位信息 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysPost",
        configuration = HystrixCon.class, fallback = ISysPostServiceImpl.class)
public interface ISysPostService
{
    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位列表
     */
    @RequestMapping(value = "/selectPostList", method = RequestMethod.GET)
    List<SysPost> selectPostList(@RequestBody SysPost post);

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    @RequestMapping(value = "/selectPostAll", method = RequestMethod.GET)
    List<SysPost> selectPostAll();

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @RequestMapping(value = "/selectPostById", method = RequestMethod.GET)
    SysPost selectPostById(@RequestParam("postId") Long postId);

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    @RequestMapping(value = "/selectPostListByUserId", method = RequestMethod.GET)
    List<Long> selectPostListByUserId(@RequestParam("postId") Long userId);

    /**
     * 校验岗位名称
     *
     * @param post 岗位信息
     * @return 结果
     */
    @RequestMapping(value = "/checkPostNameUnique", method = RequestMethod.GET)
    String checkPostNameUnique(@RequestBody SysPost post);

    /**
     * 校验岗位编码
     *
     * @param post 岗位信息
     * @return 结果
     */
    @RequestMapping(value = "/checkPostCodeUnique", method = RequestMethod.GET)
    String checkPostCodeUnique(@RequestBody SysPost post);

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @RequestMapping(value = "/countUserPostById", method = RequestMethod.GET)
    int countUserPostById(@RequestParam("postId") Long postId);

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @RequestMapping(value = "/deletePostById", method = RequestMethod.DELETE)
    int deletePostById(@RequestParam("postId") Long postId);

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     * @throws Exception 异常
     */
    @RequestMapping(value = "/deletePostByIds", method = RequestMethod.DELETE)
    int deletePostByIds(@RequestParam("postIds") Long[] postIds);

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @RequestMapping(value = "/insertPost", method = RequestMethod.POST)
    int insertPost(@RequestBody SysPost post);

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @RequestMapping(value = "/updatePost", method = RequestMethod.PUT)
    int updatePost(@RequestBody SysPost post);
}
