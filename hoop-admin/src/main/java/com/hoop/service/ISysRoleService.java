package com.hoop.service;

import com.hoop.common.core.domain.entity.SysRole;
import com.hoop.config.HystrixCon;
import com.hoop.domain.SysUserRole;
import com.hoop.service.impl.ISysRoleServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

/**
 * 角色业务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysRole",
        configuration = HystrixCon.class, fallback = ISysRoleServiceImpl.class)
public interface ISysRoleService {
    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @RequestMapping(value = "/selectRoleList", method = RequestMethod.GET)
    List<SysRole> selectRoleList(@RequestBody SysRole role);

    /**
     * 根据用户ID查询角色列表
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    @RequestMapping(value = "/selectRolesByUserId", method = RequestMethod.GET)
    List<SysRole> selectRolesByUserId(@RequestParam("userId") Long userId);

    /**
     * 根据用户ID查询角色权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @RequestMapping(value = "/selectRolePermissionByUserId", method = RequestMethod.GET)
    Set<String> selectRolePermissionByUserId(@RequestParam("userId") Long userId);

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    @RequestMapping(value = "/selectRoleAll", method = RequestMethod.GET)
    List<SysRole> selectRoleAll();

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    @RequestMapping(value = "/selectRoleListByUserId", method = RequestMethod.GET)
    List<Long> selectRoleListByUserId(@RequestParam("userId") Long userId);

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @RequestMapping(value = "/selectRoleById", method = RequestMethod.GET)
    SysRole selectRoleById(@RequestParam("roleId") Long roleId);

    /**
     * 校验角色名称是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/checkRoleNameUnique", method = RequestMethod.GET)
    String checkRoleNameUnique(@RequestBody SysRole role);

    /**
     * 校验角色权限是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/checkRoleKeyUnique", method = RequestMethod.GET)
    String checkRoleKeyUnique(@RequestBody SysRole role);

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    @RequestMapping(value = "/checkRoleAllowed", method = RequestMethod.GET)
    void checkRoleAllowed(@RequestBody SysRole role);

    /**
     * 校验角色是否有数据权限
     *
     * @param roleId 角色id
     */
    @RequestMapping(value = "/checkRoleDataScope", method = RequestMethod.GET)
    void checkRoleDataScope(@RequestParam("roleId") Long roleId);

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @RequestMapping(value = "/countUserRoleByRoleId", method = RequestMethod.GET)
    int countUserRoleByRoleId(@RequestParam("roleId") Long roleId);

    /**
     * 新增保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/insertRole", method = RequestMethod.POST)
    int insertRole(@RequestBody SysRole role);

    /**
     * 修改保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/updateRole", method = RequestMethod.PUT)
    int updateRole(@RequestBody SysRole role);

    /**
     * 修改角色状态
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/updateRoleStatus", method = RequestMethod.PUT)
    int updateRoleStatus(@RequestBody SysRole role);

    /**
     * 修改数据权限信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @RequestMapping(value = "/authDataScope", method = RequestMethod.PUT)
    int authDataScope(@RequestBody SysRole role);

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteRoleById", method = RequestMethod.DELETE)
    int deleteRoleById(@RequestParam("roleId") Long roleId);

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */

    @RequestMapping(value = "/deleteRoleByIds", method = RequestMethod.DELETE)
    int deleteRoleByIds(@RequestParam("roleIds") Long[] roleIds);

    /**
     * 取消授权用户角色
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    @RequestMapping(value = "/deleteAuthUser", method = RequestMethod.DELETE)
    int deleteAuthUser(@RequestBody SysUserRole userRole);

    /**
     * 批量取消授权用户角色
     *
     * @param roleId  角色ID
     * @param userIds 需要取消授权的用户数据ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteAuthUsers", method = RequestMethod.DELETE)
    int deleteAuthUsers(@RequestParam("roleId") Long roleId, @RequestParam("userIds") Long[] userIds);

    /**
     * 批量选择授权用户角色
     *
     * @param roleId  角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @RequestMapping(value = "/insertAuthUsers", method = RequestMethod.POST)
    int insertAuthUsers(@RequestParam("roleId") Long roleId,@RequestParam("userIds") Long[] userIds);
}
