package com.hoop.service;

import com.hoop.config.HystrixCon;

import com.hoop.domain.SysNotice;
import com.hoop.service.impl.ISysNoticeServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 公告 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysNotice",
        configuration = HystrixCon.class, fallback = ISysNoticeServiceImpl.class)
public interface ISysNoticeService
{
    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @RequestMapping(value = "/selectNoticeById", method = RequestMethod.GET)
    public SysNotice selectNoticeById(@RequestParam("noticeId") Long noticeId);

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @RequestMapping(value = "/selectNoticeList", method = RequestMethod.GET)
    public List<SysNotice> selectNoticeList(@RequestBody SysNotice notice);

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @RequestMapping(value = "/insertNotice", method = RequestMethod.POST)
    public int insertNotice(@RequestBody SysNotice notice);

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @RequestMapping(value = "/updateNotice", method = RequestMethod.PUT)
    public int updateNotice(@RequestBody SysNotice notice);

    /**
     * 删除公告信息
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteNoticeById", method = RequestMethod.DELETE)
    public int deleteNoticeById(@RequestParam("noticeId") Long noticeId);

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteNoticeByIds", method = RequestMethod.DELETE)
    public int deleteNoticeByIds(@RequestParam("noticeIds") Long[] noticeIds);
}
