package com.hoop.service;

import com.hoop.common.core.domain.TreeSelect;
import com.hoop.common.core.domain.entity.SysMenu;
import com.hoop.config.HystrixCon;

import com.hoop.domain.vo.RouterVo;
import com.hoop.service.impl.ISysMenuServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

/**
 * 菜单 业务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysMenu",
        configuration = HystrixCon.class, fallback = ISysMenuServiceImpl.class)
public interface ISysMenuService
{

    /**
     * 根据用户查询系统菜单列表
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    @RequestMapping(value = "/selectMenuList1", method = RequestMethod.GET)
    List<SysMenu> selectMenuList(@RequestParam("userId") Long userId);

    /**
     * 根据用户查询系统菜单列表
     *
     * @param menu 菜单信息
     * @param userId 用户ID
     * @return 菜单列表
     */
    @RequestMapping(value = "/selectMenuList2", method = RequestMethod.GET)
    List<SysMenu> selectMenuList(@RequestBody SysMenu menu,@RequestParam("userId") Long userId);
    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @RequestMapping(value = "/selectMenuPermsByUserId", method = RequestMethod.GET)
    Set<String> selectMenuPermsByUserId(@RequestParam("userId") Long userId);

    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    @RequestMapping(value = "/selectMenuTreeByUserId", method = RequestMethod.GET)
    List<SysMenu> selectMenuTreeByUserId(@RequestParam("userId") Long userId);

    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    @RequestMapping(value = "/selectMenuListByRoleId", method = RequestMethod.GET)
    List<Long> selectMenuListByRoleId(@RequestParam("roleId") Long roleId);

    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    @RequestMapping(value = "/buildMenus", method = RequestMethod.GET)
    List<RouterVo> buildMenus(@RequestParam("menus") List<SysMenu> menus);

    /**
     * 构建前端所需要树结构
     *
     * @param menus 菜单列表
     * @return 树结构列表
     */
    @RequestMapping(value = "/buildMenuTree", method = RequestMethod.GET)
    List<SysMenu> buildMenuTree(@RequestParam("menus") List<SysMenu> menus);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    @RequestMapping(value = "/buildMenuTreeSelect", method = RequestMethod.GET)
    List<TreeSelect> buildMenuTreeSelect(@RequestParam("menus") List<SysMenu> menus);

    /**
     * 根据菜单ID查询信息
     *
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    @RequestMapping(value = "/selectMenuById", method = RequestMethod.GET)
    SysMenu selectMenuById(@RequestParam("menuId") Long menuId);

    /**
     * 是否存在菜单子节点
     *
     * @param menuId 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    @RequestMapping(value = "/hasChildByMenuId", method = RequestMethod.GET)
    boolean hasChildByMenuId(@RequestParam("menuId") Long menuId);

    /**
     * 查询菜单是否存在角色
     *
     * @param menuId 菜单ID
     * @return 结果 true 存在 false 不存在
     */
    @RequestMapping(value = "/checkMenuExistRole", method = RequestMethod.GET)
    boolean checkMenuExistRole(@RequestParam("menuId") Long menuId);

    /**
     * 新增保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    @RequestMapping(value = "/insertMenu", method = RequestMethod.POST)
    int insertMenu(@RequestBody() SysMenu menu);

    /**
     * 修改保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    @RequestMapping(value = "/updateMenu", method = RequestMethod.PUT)
    int updateMenu(@RequestBody SysMenu menu);

    /**
     * 删除菜单管理信息
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteMenuById", method = RequestMethod.DELETE)
    int deleteMenuById(@RequestParam("menuId") Long menuId);

    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    @RequestMapping(value = "/checkMenuNameUnique", method = RequestMethod.GET)
    String checkMenuNameUnique(@RequestBody SysMenu menu);
}
