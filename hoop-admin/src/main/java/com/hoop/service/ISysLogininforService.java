package com.hoop.service;

import com.hoop.config.HystrixCon;
import com.hoop.domain.SysLogininfor;
import com.hoop.service.impl.ISysLogininforServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统访问日志情况信息 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysLogininfor",
        configuration = HystrixCon.class, fallback = ISysLogininforServiceImpl.class)
public interface ISysLogininforService
{
    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    @RequestMapping(value = "/insertLogininfor", method = RequestMethod.POST)
    void insertLogininfor(@RequestBody SysLogininfor logininfor);

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @RequestMapping(value = "/selectLogininforList", method = RequestMethod.GET)
    List<SysLogininfor> selectLogininforList(@RequestBody SysLogininfor logininfor);

    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return
     */
    @RequestMapping(value = "/deleteLogininforByIds", method = RequestMethod.DELETE)
    int deleteLogininforByIds(@RequestParam("infoIds") Long[] infoIds);

    /**
     * 清空系统登录日志
     */
    @RequestMapping(value = "/cleanLogininfor", method = RequestMethod.DELETE)
    void cleanLogininfor();
}
