package com.hoop.service;

import com.hoop.common.core.domain.entity.SysUser;
import com.hoop.config.HystrixCon;
import com.hoop.service.impl.ISysUserServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 用户 业务层
 *
 * @author dongua
 */

@Service
@FeignClient(name = "hoop-provider", contextId = "SysUser",
        configuration = HystrixCon.class, fallback = ISysUserServiceImpl.class)
public interface ISysUserService {
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @RequestMapping(value = "/selectUserList1", method = RequestMethod.GET)
    List<SysUser> selectUserList(@RequestBody SysUser user);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @RequestMapping(value = "/selectAllocatedList", method = RequestMethod.GET)
    List<SysUser> selectAllocatedList(@RequestBody SysUser user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @RequestMapping(value = "/selectUnallocatedList", method = RequestMethod.GET)
    List<SysUser> selectUnallocatedList(@RequestBody SysUser user);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @RequestMapping(value = "/selectUserByUserName", method = RequestMethod.GET)
    SysUser selectUserByUserName(@RequestParam("userName") String userName);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @RequestMapping(value = "/selectUserById", method = RequestMethod.GET)
    SysUser selectUserById(@RequestParam("userId") Long userId);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @RequestMapping(value = "/selectUserRoleGroup", method = RequestMethod.GET)
    String selectUserRoleGroup(@RequestParam("userName") String userName);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    @RequestMapping(value = "/selectUserPostGroup", method = RequestMethod.GET)
    String selectUserPostGroup(@RequestParam("userName") String userName);

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    @RequestMapping(value = "/checkUserNameUnique", method = RequestMethod.GET)
    String checkUserNameUnique(@RequestParam("userName") String userName);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/checkPhoneUnique", method = RequestMethod.GET)
    String checkPhoneUnique(@RequestBody SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/checkEmailUnique", method = RequestMethod.GET)
    String checkEmailUnique(@RequestBody SysUser user);

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    @RequestMapping(value = "/checkUserAllowed", method = RequestMethod.GET)
    void checkUserAllowed(@RequestBody SysUser user);

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    @RequestMapping(value = "/checkUserDataScope", method = RequestMethod.GET)
    void checkUserDataScope(@RequestParam("userId") Long userId);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/insertUser", method = RequestMethod.POST)
    int insertUser(@RequestBody SysUser user);

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    boolean registerUser(@RequestBody SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    int updateUser(@RequestBody SysUser user);

    /**
     * 用户授权角色
     *
     * @param userId  用户ID
     * @param roleIds 角色组
     */
    @RequestMapping(value = "/insertUserAuth", method = RequestMethod.POST)
    void insertUserAuth(@RequestParam("userId") Long userId, @RequestParam("roleIds") Long[] roleIds);

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/updateUserStatus", method = RequestMethod.PUT)
    int updateUserStatus(@RequestBody SysUser user);

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/updateUserProfile", method = RequestMethod.PUT)
    int updateUserProfile(@RequestBody SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    @RequestMapping(value = "/updateUserAvatar", method = RequestMethod.PUT)
    boolean updateUserAvatar(@RequestParam("userName") String userName, @RequestParam("avatar") String avatar);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @RequestMapping(value = "/resetPwd", method = RequestMethod.PUT)
    int resetPwd(@RequestBody SysUser user);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @RequestMapping(value = "/resetUserPwd", method = RequestMethod.PUT)
    int resetUserPwd(@RequestParam("userName") String userName, @RequestParam("password") String password);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteUserById", method = RequestMethod.DELETE)
    int deleteUserById(@RequestParam("userId") Long userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */

    @RequestMapping(value = "/deleteUserByIds", method = RequestMethod.DELETE)
    int deleteUserByIds(@RequestParam("userIds") Long[] userIds);

    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @RequestMapping(value = "/selectUserList2", method = RequestMethod.GET)
    String importUser(@RequestParam("userList") List<SysUser> userList, @RequestParam("isUpdateSupport") Boolean isUpdateSupport, @RequestParam("operName") String operName);
}
