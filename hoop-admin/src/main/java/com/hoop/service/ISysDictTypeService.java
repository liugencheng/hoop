package com.hoop.service;

import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.common.core.domain.entity.SysDictType;
import com.hoop.config.HystrixCon;
import com.hoop.service.impl.ISysDictTypeServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysDictType",
        configuration = HystrixCon.class, fallback = ISysDictTypeServiceImpl.class)
public interface ISysDictTypeService
{
    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    @RequestMapping(value = "/selectDictTypeList", method = RequestMethod.GET)
    public List<SysDictType> selectDictTypeList(@RequestBody SysDictType dictType);

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    @RequestMapping(value = "/selectDictTypeAll", method = RequestMethod.GET)
    public List<SysDictType> selectDictTypeAll();

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @RequestMapping(value = "/selectDictDataByType", method = RequestMethod.GET)
    public List<SysDictData> selectDictDataByType(@RequestParam("dictType") String dictType);

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    @RequestMapping(value = "/selectDictTypeById", method = RequestMethod.GET)
    public SysDictType selectDictTypeById(@RequestParam("dictId") Long dictId);

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    @RequestMapping(value = "/selectDictTypeByType", method = RequestMethod.GET)
    public SysDictType selectDictTypeByType(@RequestParam("dictType") String dictType);

    /**
     * 批量删除字典信息
     *
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteDictTypeByIds", method = RequestMethod.DELETE)
    public void deleteDictTypeByIds(@RequestParam("dictIds") Long[] dictIds);

    /**
     * 加载字典缓存数据
     */
    @RequestMapping(value = "/loadingDictCache", method = RequestMethod.GET)
    public void loadingDictCache();

    /**
     * 清空字典缓存数据
     */
    @RequestMapping(value = "/clearDictCache", method = RequestMethod.GET)
    public void clearDictCache();

    /**
     * 重置字典缓存数据
     */
    @RequestMapping(value = "/resetDictCache", method = RequestMethod.GET)
    public void resetDictCache();

    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @RequestMapping(value = "/insertDictType", method = RequestMethod.POST)
    public int insertDictType(@RequestBody SysDictType dictType);

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @RequestMapping(value = "/updateDictType", method = RequestMethod.PUT)
    public int updateDictType(@RequestBody SysDictType dictType);

    /**
     * 校验字典类型称是否唯一
     *
     * @param dictType 字典类型
     * @return 结果
     */
    @RequestMapping(value = "/checkDictTypeUnique", method = RequestMethod.GET)
    public String checkDictTypeUnique(@RequestBody SysDictType dictType);
}
