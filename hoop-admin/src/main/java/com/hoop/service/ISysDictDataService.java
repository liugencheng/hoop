package com.hoop.service;

import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.config.HystrixCon;
import com.hoop.service.impl.ISysDictDataServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysDictData",
        configuration = HystrixCon.class, fallback = ISysDictDataServiceImpl.class)
public interface ISysDictDataService
{
    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @RequestMapping(value = "/selectDictDataList", method = RequestMethod.GET)
    public List<SysDictData> selectDictDataList(@RequestBody SysDictData dictData);

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @RequestMapping(value = "/selectDictLabel", method = RequestMethod.GET)
    public String selectDictLabel(@RequestParam("dictType") String dictType, @RequestParam("dictValue") String dictValue);

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @RequestMapping(value = "/selectDictDataById", method = RequestMethod.GET)
    public SysDictData selectDictDataById(@RequestParam("dictCode") Long dictCode);

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteDictDataByIds", method = RequestMethod.DELETE)
    public void deleteDictDataByIds(@RequestParam("dictCodes") Long[] dictCodes);

    /**
     * 新增保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @RequestMapping(value = "/insertDictData", method = RequestMethod.POST)
    public int insertDictData(@RequestBody SysDictData dictData);

    /**
     * 修改保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @RequestMapping(value = "/updateDictData", method = RequestMethod.PUT)
    public int updateDictData(@RequestBody SysDictData dictData);
}
