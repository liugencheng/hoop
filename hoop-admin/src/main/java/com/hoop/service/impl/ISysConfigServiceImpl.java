package com.hoop.service.impl;

import com.hoop.domain.SysConfig;
import com.hoop.service.ISysConfigService;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author dongua
 * @date 2022/6/22 16:32
 **/
//@Component
public class ISysConfigServiceImpl implements ISysConfigService {
    @Override
    public SysConfig selectConfigById(Long configId) {
        System.out.println("selectConfigById:出错了");
        return null;
    }

    @Override
    public String selectConfigByKey(String configKey) {
        System.out.println("selectConfigByKey:出错了");
        return null;
    }

    @Override
    public boolean selectCaptchaOnOff() {
        System.out.println("selectCaptchaOnOff:出错了");
        return false;
    }

    @Override
    public List<SysConfig> selectConfigList(SysConfig config) {
        System.out.println("selectConfigList:出错了");
        return null;
    }

    @Override
    public int insertConfig(SysConfig config) {
        System.out.println("insertConfig:出错了");
        return 0;
    }

    @Override
    public int updateConfig(SysConfig config) {
        System.out.println("updateConfig:出错了");
        return 0;
    }

    @Override
    public void deleteConfigByIds(Long[] configIds) {
        System.out.println("deleteConfigByIds:出错了");
    }

    @Override
    public void loadingConfigCache() {
        System.out.println("loadingConfigCache:出错了");
    }

    @Override
    public void clearConfigCache() {
        System.out.println("clearConfigCache:出错了");
    }

    @Override
    public void resetConfigCache() {
        System.out.println("resetConfigCache:出错了");
    }

    @Override
    public String checkConfigKeyUnique(SysConfig config) {
        System.out.println("checkConfigKeyUnique:出错了");
        return null;
    }
}
