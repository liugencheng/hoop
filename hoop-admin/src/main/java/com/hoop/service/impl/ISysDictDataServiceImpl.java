package com.hoop.service.impl;

import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.service.ISysDictDataService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author dongua
 * @date 2022/6/22 16:55
 **/
@Component
public class ISysDictDataServiceImpl implements ISysDictDataService {
    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData) {
        System.out.println("selectDictDataList:出错了");
        return null;
    }

    @Override
    public String selectDictLabel(String dictType, String dictValue) {
        System.out.println("selectDictLabel:出错了");
        return null;
    }

    @Override
    public SysDictData selectDictDataById(Long dictCode) {
        System.out.println("selectDictDataById:出错了");
        return null;
    }

    @Override
    public void deleteDictDataByIds(Long[] dictCodes) {
        System.out.println("deleteDictDataByIds:出错了");
    }

    @Override
    public int insertDictData(SysDictData dictData) {
        System.out.println("insertDictData:出错了");
        return 0;
    }

    @Override
    public int updateDictData(SysDictData dictData) {
        System.out.println("updateDictData:出错了");
        return 0;
    }
}
