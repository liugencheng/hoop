package com.hoop.service.impl;

import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.common.core.domain.entity.SysDictType;
import com.hoop.service.ISysDictTypeService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author dongua
 * @date 2022/6/22 16:56
 **/
@Component
public class ISysDictTypeServiceImpl implements ISysDictTypeService {
    @Override
    public List<SysDictType> selectDictTypeList(SysDictType dictType) {
        System.out.println("selectDictTypeList:出错了");
        return null;
    }

    @Override
    public List<SysDictType> selectDictTypeAll() {
        System.out.println("selectDictTypeAll:出错了");
        return null;
    }

    @Override
    public List<SysDictData> selectDictDataByType(String dictType) {
        System.out.println("selectDictDataByType:出错了");
        return null;
    }

    @Override
    public SysDictType selectDictTypeById(Long dictId) {
        System.out.println("selectDictTypeById:出错了");
        return null;
    }

    @Override
    public SysDictType selectDictTypeByType(String dictType) {
        System.out.println("selectDictTypeByType:出错了");
        return null;
    }

    @Override
    public void deleteDictTypeByIds(Long[] dictIds) {
        System.out.println("deleteDictTypeByIds:出错了");
    }

    @Override
    public void loadingDictCache() {
        System.out.println("loadingDictCache:出错了");
    }

    @Override
    public void clearDictCache() {
        System.out.println("clearDictCache:出错了");
    }

    @Override
    public void resetDictCache() {
        System.out.println("resetDictCache:出错了");
    }

    @Override
    public int insertDictType(SysDictType dictType) {
        System.out.println("insertDictType:出错了");
        return 0;
    }

    @Override
    public int updateDictType(SysDictType dictType) {
        System.out.println("updateDictType:出错了");
        return 0;
    }

    @Override
    public String checkDictTypeUnique(SysDictType dictType) {
        System.out.println("checkDictTypeUnique:出错了");
        return null;
    }
}
