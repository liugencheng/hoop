package com.hoop.service.impl;

import com.hoop.common.core.domain.TreeSelect;
import com.hoop.common.core.domain.entity.SysDept;
import com.hoop.service.ISysDeptService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author dongua
 * @date 2022/6/22 16:54
 **/
@Component
public class ISysDeptServiceImpl implements ISysDeptService {
    @Override
    public List<SysDept> selectDeptList(SysDept dept) {
        System.out.println("selectDeptList:出错了");
        return null;
    }

    @Override
    public List<SysDept> buildDeptTree(List<SysDept> depts) {
        System.out.println("buildDeptTree:出错了");
        return null;
    }

    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts) {
        System.out.println("buildDeptTreeSelect:出错了");
        return null;
    }

    @Override
    public List<Long> selectDeptListByRoleId(Long roleId) {
        System.out.println("selectDeptListByRoleId:出错了");
        return null;
    }

    @Override
    public SysDept selectDeptById(Long deptId) {
        System.out.println("selectDeptById:出错了");
        return null;
    }

    @Override
    public int selectNormalChildrenDeptById(Long deptId) {
        System.out.println("selectNormalChildrenDeptById:出错了");
        return 0;
    }

    @Override
    public boolean hasChildByDeptId(Long deptId) {
        System.out.println("hasChildByDeptId:出错了");
        return false;
    }

    @Override
    public boolean checkDeptExistUser(Long deptId) {
        System.out.println("checkDeptExistUser:出错了");
        return false;
    }

    @Override
    public String checkDeptNameUnique(SysDept dept) {
        System.out.println("checkDeptNameUnique:出错了");
        return null;
    }

    @Override
    public void checkDeptDataScope(Long deptId) {
        System.out.println("checkDeptDataScope:出错了");
    }

    @Override
    public int insertDept(SysDept dept) {
        System.out.println("insertDept:出错了");
        return 0;
    }

    @Override
    public int updateDept(SysDept dept) {
        System.out.println("updateDept:出错了");
        return 0;
    }

    @Override
    public int deleteDeptById(Long deptId) {
        System.out.println("deleteDeptById:出错了");
        return 0;
    }
}
