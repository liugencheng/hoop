package com.hoop.service.impl;

import com.hoop.common.core.domain.model.LoginUser;
import com.hoop.domain.SysUserOnline;
import com.hoop.service.ISysUserOnlineService;

import org.springframework.stereotype.Component;

/**
 * @author JZW
 * @date 2022/6/22 17:10
 **/
@Component
public class ISysUserOnlineServiceImpl implements ISysUserOnlineService {
    @Override
    public SysUserOnline selectOnlineByIpaddr(String ipaddr, LoginUser user) {
        System.out.println("selectOnlineByIpaddr:出错了");
        return null;
    }

    @Override
    public SysUserOnline selectOnlineByUserName(String userName, LoginUser user) {
        System.out.println("selectOnlineByUserName:出错了");
        return null;
    }

    @Override
    public SysUserOnline selectOnlineByInfo(String ipaddr, String userName, LoginUser user) {
        System.out.println("selectOnlineByInfo:出错了");
        return null;
    }

    @Override
    public SysUserOnline loginUserToUserOnline(LoginUser user) {
        System.out.println("loginUserToUserOnline:出错了");
        return null;
    }
}
