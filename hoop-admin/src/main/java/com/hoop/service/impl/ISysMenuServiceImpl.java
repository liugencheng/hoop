package com.hoop.service.impl;

import com.hoop.common.core.domain.TreeSelect;
import com.hoop.common.core.domain.entity.SysMenu;
import com.hoop.domain.vo.RouterVo;
import com.hoop.service.ISysMenuService;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
@Component
public class ISysMenuServiceImpl implements ISysMenuService {

    @Override
    public List<SysMenu> selectMenuList(Long userId) {
        System.out.println("selectMenuList:出错了");
        return null;
    }

    @Override
    public List<SysMenu> selectMenuList(SysMenu menu, Long userId) {
        System.out.println("selectMenuList:出错了");
        return null;
    }
    @Override
    public Set<String> selectMenuPermsByUserId(Long userId) {
        System.out.println("selectMenuPermsByUserId:出错了");
        return null;
    }



    @Override
    public List<SysMenu> selectMenuTreeByUserId(Long userId) {
        System.out.println("selectMenuTreeByUserId:出错了");
        return null;
    }

    @Override
    public List<Long> selectMenuListByRoleId(Long roleId) {
        System.out.println("selectMenuListByRoleId:出错了");
        return null;
    }

    @Override
    public List<RouterVo> buildMenus(List<SysMenu> menus) {
        System.out.println("buildMenus:出错了");
        return null;
    }

    @Override
    public List<SysMenu> buildMenuTree(List<SysMenu> menus) {
        System.out.println("buildMenuTree:出错了");
        return null;
    }

    @Override
    public List<TreeSelect> buildMenuTreeSelect(List<SysMenu> menus) {
        System.out.println("buildMenuTreeSelect:出错了");
        return null;
    }

    @Override
    public SysMenu selectMenuById(Long menuId) {
        System.out.println("selectMenuById:出错了");
        return null;
    }

    @Override
    public boolean hasChildByMenuId(Long menuId) {
        System.out.println("hasChildByMenuId:出错了");
        return false;
    }

    @Override
    public boolean checkMenuExistRole(Long menuId) {
        System.out.println("checkMenuExistRole:出错了");
        return false;
    }

    @Override
    public int insertMenu(SysMenu menu) {
        System.out.println("insertMenu:出错了");
        return 0;
    }

    @Override
    public int updateMenu(SysMenu menu) {
        System.out.println("updateMenu:出错了");
        return 0;
    }

    @Override
    public int deleteMenuById(Long menuId) {
        System.out.println("deleteMenuById:出错了");
        return 0;
    }

    @Override
    public String checkMenuNameUnique(SysMenu menu) {
        System.out.println("checkMenuNameUnique:出错了");
        return null;
    }
}
