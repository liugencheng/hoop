package com.hoop.service.impl;

import com.hoop.common.core.domain.entity.SysUser;
import com.hoop.service.ISysUserService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author JZW
 * @date 2022/6/22 16:48
 **/
@Component
public class ISysUserServiceImpl implements ISysUserService {

    @Override
    public List<SysUser> selectUserList(SysUser user) {
        System.out.println("selectUserList:出错了");
        return null;
    }

    @Override
    public List<SysUser> selectAllocatedList(SysUser user) {
        System.out.println("selectAllocatedList:出错了");
        return null;
    }

    @Override
    public List<SysUser> selectUnallocatedList(SysUser user) {
        System.out.println("selectUnallocatedList:出错了");
        return null;
    }

    @Override
    public SysUser selectUserByUserName(String userName) {
        System.out.println("selectUserByUserName:出错了");
        return null;
    }

    @Override
    public SysUser selectUserById(Long userId) {
        System.out.println("selectUserById:出错了");
        return null;
    }

    @Override
    public String selectUserRoleGroup(String userName) {
        System.out.println("selectUserRoleGroup:出错了");
        return null;
    }

    @Override
    public String selectUserPostGroup(String userName) {
        System.out.println("selectUserPostGroup:出错了");
        return null;
    }

    @Override
    public String checkUserNameUnique(String userName) {
        System.out.println("checkUserNameUnique:出错了");
        return null;
    }

    @Override
    public String checkPhoneUnique(SysUser user) {
        System.out.println("checkPhoneUnique:出错了");
        return null;
    }

    @Override
    public String checkEmailUnique(SysUser user) {
        System.out.println("checkEmailUnique:出错了");
        return null;
    }

    @Override
    public void checkUserAllowed(SysUser user) {
        System.out.println("checkUserAllowed:出错了");

    }

    @Override
    public void checkUserDataScope(Long userId) {
        System.out.println("checkUserDataScope:出错了");

    }

    @Override
    public int insertUser(SysUser user) {
        System.out.println("insertUser:出错了");
        return 0;
    }

    @Override
    public boolean registerUser(SysUser user) {
        System.out.println("registerUser:出错了");
        return false;
    }

    @Override
    public int updateUser(SysUser user) {
        System.out.println("updateUser:出错了");
        return 0;
    }

    @Override
    public void insertUserAuth(Long userId, Long[] roleIds) {
        System.out.println("insertUserAuth:出错了");

    }

    @Override
    public int updateUserStatus(SysUser user) {
        System.out.println("updateUserStatus:出错了");
        return 0;
    }

    @Override
    public int updateUserProfile(SysUser user) {
        System.out.println("updateUserProfile:出错了");
        return 0;
    }

    @Override
    public boolean updateUserAvatar(String userName, String avatar) {
        System.out.println("updateUserAvatar:出错了");
        return false;
    }

    @Override
    public int resetPwd(SysUser user) {
        System.out.println("resetPwd:出错了");
        return 0;
    }

    @Override
    public int resetUserPwd(String userName, String password) {
        System.out.println("resetUserPwd:出错了");
        return 0;
    }

    @Override
    public int deleteUserById(Long userId) {
        System.out.println("deleteUserById:出错了");
        return 0;
    }

    @Override
    public int deleteUserByIds(Long[] userIds) {
        System.out.println("deleteUserByIds:出错了");
        return 0;
    }

    @Override
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName) {
        System.out.println("importUser:出错了");
        return null;
    }
}
