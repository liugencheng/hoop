package com.hoop.service.impl;

import com.hoop.domain.SysNotice;
import com.hoop.service.ISysNoticeService;

import org.springframework.stereotype.Component;

import java.util.List;
/**
 * @author admin
 */
@Component
public class ISysNoticeServiceImpl implements ISysNoticeService {
    @Override
    public SysNotice selectNoticeById(Long noticeId) {
        System.out.println("selectNoticeById:出错了");
        return null;
    }

    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice) {
        System.out.println("selectNoticeList:出错了");
        return null;
    }

    @Override
    public int insertNotice(SysNotice notice) {
        System.out.println("insertNotice:出错了");
        return 0;
    }

    @Override
    public int updateNotice(SysNotice notice) {
        System.out.println("updateNotice:出错了");
        return 0;
    }

    @Override
    public int deleteNoticeById(Long noticeId) {
        System.out.println("deleteNoticeById:出错了");
        return 0;
    }

    @Override
    public int deleteNoticeByIds(Long[] noticeIds) {
        System.out.println("deleteNoticeByIds:出错了");
        return 0;
    }
}
