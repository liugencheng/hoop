package com.hoop.service.impl;

import com.hoop.domain.SysPost;
import com.hoop.service.ISysPostService;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author JZW
 * @date 2022/6/22 17:27
 **/
@Component
public class ISysPostServiceImpl implements ISysPostService {
    @Override
    public List<SysPost> selectPostList(SysPost post) {
        System.out.println("selectPostList:出错了");
        return null;
    }

    @Override
    public List<SysPost> selectPostAll() {
        System.out.println("selectPostAll:出错了");
        return null;
    }

    @Override
    public SysPost selectPostById(Long postId) {
        System.out.println("selectPostById:出错了");
        return null;
    }

    @Override
    public List<Long> selectPostListByUserId(Long userId) {
        System.out.println("selectPostListByUserId:出错了");
        return null;
    }

    @Override
    public String checkPostNameUnique(SysPost post) {
        System.out.println("checkPostNameUnique:出错了");
        return null;
    }

    @Override
    public String checkPostCodeUnique(SysPost post) {
        System.out.println("checkPostCodeUnique:出错了");
        return null;
    }

    @Override
    public int countUserPostById(Long postId) {
        System.out.println("countUserPostById:出错了");
        return 0;
    }

    @Override
    public int deletePostById(Long postId) {
        System.out.println("deletePostById:出错了");
        return 0;
    }

    @Override
    public int deletePostByIds(Long[] postIds) {
        System.out.println("deletePostByIds:出错了");
        return 0;
    }

    @Override
    public int insertPost(SysPost post) {
        System.out.println("insertPost:出错了");
        return 0;
    }

    @Override
    public int updatePost(SysPost post) {
        System.out.println("updatePost:出错了");
        return 0;
    }
}
