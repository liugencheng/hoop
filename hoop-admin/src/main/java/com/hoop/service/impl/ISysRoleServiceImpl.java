package com.hoop.service.impl;

import com.hoop.common.core.domain.entity.SysRole;
import com.hoop.domain.SysUserRole;
import com.hoop.service.ISysRoleService;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @author JZW
 * @date 2022/6/22 17:16
 **/
@Component
public class ISysRoleServiceImpl implements ISysRoleService {
    @Override
    public List<SysRole> selectRoleList(SysRole role) {
        System.out.println("selectRoleList:出错了");
        return null;
    }

    @Override
    public List<SysRole> selectRolesByUserId(Long userId) {
        System.out.println("selectRolesByUserId:出错了");
        return null;
    }

    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        System.out.println("selectRolePermissionByUserId:出错了");
        return null;
    }

    @Override
    public List<SysRole> selectRoleAll() {
        System.out.println("selectRoleAll:出错了");
        return null;
    }

    @Override
    public List<Long> selectRoleListByUserId(Long userId) {
        System.out.println("selectRoleListByUserId:出错了");
        return null;
    }

    @Override
    public SysRole selectRoleById(Long roleId) {
        System.out.println("selectRoleById:出错了");
        return null;
    }

    @Override
    public String checkRoleNameUnique(SysRole role) {
        System.out.println("checkRoleNameUnique:出错了");
        return null;
    }

    @Override
    public String checkRoleKeyUnique(SysRole role) {
        System.out.println("checkRoleKeyUnique:出错了");
        return null;
    }

    @Override
    public void checkRoleAllowed(SysRole role) {
        System.out.println("checkRoleAllowed:出错了");

    }

    @Override
    public void checkRoleDataScope(Long roleId) {
        System.out.println("checkRoleDataScope:出错了");

    }

    @Override
    public int countUserRoleByRoleId(Long roleId) {
        System.out.println("countUserRoleByRoleId:出错了");
        return 0;
    }

    @Override
    public int insertRole(SysRole role) {
        System.out.println("insertRole:出错了");
        return 0;
    }

    @Override
    public int updateRole(SysRole role) {
        System.out.println("updateRole:出错了");
        return 0;
    }

    @Override
    public int updateRoleStatus(SysRole role) {
        System.out.println("updateRoleStatus:出错了");
        return 0;
    }

    @Override
    public int authDataScope(SysRole role) {
        System.out.println("authDataScope:出错了");
        return 0;
    }

    @Override
    public int deleteRoleById(Long roleId) {
        System.out.println("deleteRoleById:出错了");
        return 0;
    }

    @Override
    public int deleteRoleByIds(Long[] roleIds) {
        System.out.println("deleteRoleByIds:出错了");
        return 0;
    }

    @Override
    public int deleteAuthUser(SysUserRole userRole) {
        System.out.println("deleteAuthUser:出错了");
        return 0;
    }

    @Override
    public int deleteAuthUsers(Long roleId, Long[] userIds) {
        System.out.println("deleteAuthUsers:出错了");
        return 0;
    }

    @Override
    public int insertAuthUsers(Long roleId, Long[] userIds) {
        System.out.println("insertAuthUsers:出错了");
        return 0;
    }
}
