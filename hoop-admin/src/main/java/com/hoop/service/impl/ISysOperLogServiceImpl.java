package com.hoop.service.impl;

import com.hoop.domain.SysOperLog;
import com.hoop.service.ISysOperLogService;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ISysOperLogServiceImpl implements ISysOperLogService {
    @Override
    public void insertOperlog(SysOperLog operLog) {
        System.out.println("insertOperlog:出错了");
    }

    @Override
    public List<SysOperLog> selectOperLogList(SysOperLog operLog) {
        System.out.println("selectOperLogList:出错了");
        return null;
    }

    @Override
    public int deleteOperLogByIds(Long[] operIds) {
        System.out.println("deleteOperLogByIds:出错了");
        return 0;
    }

    @Override
    public SysOperLog selectOperLogById(Long operId) {
        System.out.println("selectOperLogById:出错了");
        return null;
    }

    @Override
    public void cleanOperLog() {
        System.out.println("cleanOperLog:出错了");

    }
}
