package com.hoop.service;

import com.hoop.common.core.domain.TreeSelect;
import com.hoop.common.core.domain.entity.SysDept;
import com.hoop.config.HystrixCon;
import com.hoop.service.impl.ISysDeptServiceImpl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 部门管理 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysDept",
        configuration = HystrixCon.class, fallback = ISysDeptServiceImpl.class)
public interface ISysDeptService
{
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @RequestMapping(value = "/selectDeptList", method = RequestMethod.GET)
    public List<SysDept> selectDeptList(@RequestBody SysDept dept);

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    @RequestMapping(value = "/buildDeptTree", method = RequestMethod.GET)
    public List<SysDept> buildDeptTree(@RequestParam("depts") List<SysDept> depts);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @RequestMapping(value = "/buildDeptTreeSelect", method = RequestMethod.GET)
    public List<TreeSelect> buildDeptTreeSelect(@RequestParam("depts") List<SysDept> depts);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @RequestMapping(value = "/selectDeptListByRoleId", method = RequestMethod.GET)
    public List<Long> selectDeptListByRoleId(@RequestParam("roleId") Long roleId);

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    @RequestMapping(value = "/selectDeptById", method = RequestMethod.GET)
    public SysDept selectDeptById(@RequestParam("deptId") Long deptId);

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    @RequestMapping(value = "/selectNormalChildrenDeptById", method = RequestMethod.GET)
    public int selectNormalChildrenDeptById(@RequestParam("deptId") Long deptId);

    /**
     * 是否存在部门子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @RequestMapping(value = "/hasChildByDeptId", method = RequestMethod.GET)
    public boolean hasChildByDeptId(@RequestParam("deptId") Long deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @RequestMapping(value = "/checkDeptExistUser", method = RequestMethod.GET)
    public boolean checkDeptExistUser(@RequestParam("deptId") Long deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    @RequestMapping(value = "/checkDeptNameUnique", method = RequestMethod.GET)
    public String checkDeptNameUnique(@RequestBody SysDept dept);

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    @RequestMapping(value = "/checkDeptDataScope", method = RequestMethod.GET)
    public void checkDeptDataScope(@RequestParam("deptId") Long deptId);

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @RequestMapping(value = "/insertDept", method = RequestMethod.POST)
    public int insertDept(@RequestBody SysDept dept);

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @RequestMapping(value = "/updateDept", method = RequestMethod.PUT)
    public int updateDept(@RequestBody SysDept dept);

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteDeptById", method = RequestMethod.DELETE)
    public int deleteDeptById(@RequestParam("deptId") Long deptId);
}
