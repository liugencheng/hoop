package com.hoop.service;

import com.hoop.config.FeignConfig;
import com.hoop.config.HystrixCon;
import com.hoop.domain.SysConfig;
import com.hoop.service.impl.ISysConfigServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 参数配置 服务层
 *
 * @author dongua
 */
@Service
@FeignClient(name = "hoop-provider", contextId = "SysConfig",
        configuration = FeignConfig.class)
public interface ISysConfigService
{// , fallback = ISysConfigServiceImpl.class
    /**
     * 查询参数配置信息
     *
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    @RequestMapping(value = "/selectConfigById", method = RequestMethod.GET)
    SysConfig selectConfigById(@RequestParam("configId") Long configId);

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    @RequestMapping(value = "/selectConfigByKey", method = RequestMethod.GET)
    String selectConfigByKey(@RequestParam("configKey") String configKey);

    /**
     * 获取验证码开关
     *
     * @return true开启，false关闭
     */
    @RequestMapping(value = "/selectCaptchaOnOff", method = RequestMethod.GET)
    boolean selectCaptchaOnOff();

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    @RequestMapping(value = "/selectConfigList", method = RequestMethod.GET)
    List<SysConfig> selectConfigList(@RequestBody SysConfig config);

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @RequestMapping(value = "/insertConfig", method = RequestMethod.POST)
    int insertConfig(@RequestBody SysConfig config);

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    @RequestMapping(value = "/updateConfig", method = RequestMethod.PUT)
    int updateConfig(@RequestBody SysConfig config);

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     * @return 结果
     */
    @RequestMapping(value = "/deleteConfigByIds", method = RequestMethod.DELETE)
    void deleteConfigByIds(@RequestParam("configIds") Long[] configIds);

    /**
     * 加载参数缓存数据
     */
    @RequestMapping(value = "/loadingConfigCache", method = RequestMethod.GET)
    void loadingConfigCache();

    /**
     * 清空参数缓存数据
     */
    @RequestMapping(value = "/clearConfigCache", method = RequestMethod.GET)
    void clearConfigCache();

    /**
     * 重置参数缓存数据
     */
    @RequestMapping(value = "/resetConfigCache", method = RequestMethod.GET)
    void resetConfigCache();

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数信息
     * @return 结果
     */
    @RequestMapping(value = "/checkConfigKeyUnique", method = RequestMethod.GET)
    String checkConfigKeyUnique(@RequestBody SysConfig config);
}
