package com.hoop.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.TimeZone;

/**
 * 程序注解配置
 *
 * @author dongua
 */
@Configuration
// 表示通过aop框架暴露该代理对象,AopContext能够访问
// 防止因为使用 this 关键字调用当前对象而导致不会使用代理对象而产生问题
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径
@MapperScan("com.hoop.**.mapper")
public class ApplicationConfig {
    /**
     * 时区配置 解决 json 反序列化时区问题
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
        // 默认情况下会将 时区设置为UTC ，Jackson反序列化时间类型的底层实际上调用的是Java的 SimpleDateFormat#parse() 方法，而JVM中的时区则会根据你的操作系统来获取，所以JVM认为你的时区应该是 GMT+8 时区，
        // 而要将 UTC 时区的时间转成 GMT+8 时区的时间，就会将你传进来的时间+8个小时。
        // 或者可以调用 TimeZone.getDefault() 函数直接返回的就是 JVM 时区，也就是 GMT+8 时区
        return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
    }
}
