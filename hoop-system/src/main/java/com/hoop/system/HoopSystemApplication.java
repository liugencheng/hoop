package com.hoop.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author dongua
 * @date 2022/6/22 10:36
 **/

@SpringBootApplication(scanBasePackages = {"com.hoop"}, exclude = SecurityAutoConfiguration.class)
@EnableEurekaClient
public class HoopSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(HoopSystemApplication.class, args);
    }

}
