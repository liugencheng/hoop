package com.hoop.system.service.impl;

import com.hoop.domain.SysNotice;
import com.hoop.system.mapper.SysNoticeMapper;
import com.hoop.system.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公告 服务层实现
 *
 * @author dongua
 */
@RestController
public class SysNoticeServiceImpl implements ISysNoticeService
{
    @Resource
    private SysNoticeMapper noticeMapper;

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    @RequestMapping(value = "/selectNoticeById", method = RequestMethod.GET)
    public SysNotice selectNoticeById(@RequestParam("noticeId") Long noticeId)
    {
        return noticeMapper.selectNoticeById(noticeId);
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    @RequestMapping(value = "/selectNoticeList", method = RequestMethod.GET)
    public List<SysNotice> selectNoticeList(@RequestBody SysNotice notice)
    {
        return noticeMapper.selectNoticeList(notice);
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/insertNotice", method = RequestMethod.POST)
    public int insertNotice(@RequestBody SysNotice notice)
    {
        return noticeMapper.insertNotice(notice);
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/updateNotice", method = RequestMethod.PUT)
    public int updateNotice(@RequestBody SysNotice notice)
    {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteNoticeById", method = RequestMethod.DELETE)
    public int deleteNoticeById(@RequestParam("noticeId") Long noticeId)
    {
        return noticeMapper.deleteNoticeById(noticeId);
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteNoticeByIds", method = RequestMethod.DELETE)
    public int deleteNoticeByIds(@RequestParam("noticeIds") Long[] noticeIds)
    {
        return noticeMapper.deleteNoticeByIds(noticeIds);
    }
}
