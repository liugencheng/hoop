package com.hoop.system.service.impl;

import com.hoop.domain.SysOperLog;
import com.hoop.system.mapper.SysOperLogMapper;
import com.hoop.system.service.ISysOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 操作日志 服务层处理
 *
 * @author dongua
 */
@RestController
public class SysOperLogServiceImpl implements ISysOperLogService
{
    @Resource
    private SysOperLogMapper operLogMapper;

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    @Override
    @RequestMapping(value = "/insertOperlog", method = RequestMethod.POST)
    public void insertOperlog(@RequestBody SysOperLog operLog)
    {
        operLogMapper.insertOperlog(operLog);
    }

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    @Override
    @RequestMapping(value = "/selectOperLogList", method = RequestMethod.GET)
    public List<SysOperLog> selectOperLogList(@RequestBody SysOperLog operLog)
    {
        return operLogMapper.selectOperLogList(operLog);
    }

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteOperLogByIds", method = RequestMethod.DELETE)
    public int deleteOperLogByIds(@RequestParam("operIds") Long[] operIds)
    {
        return operLogMapper.deleteOperLogByIds(operIds);
    }

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    @Override
    @RequestMapping(value = "/selectOperLogById", method = RequestMethod.GET)
    public SysOperLog selectOperLogById(@RequestParam("operId") Long operId)
    {
        return operLogMapper.selectOperLogById(operId);
    }

    /**
     * 清空操作日志
     */
    @Override
    @RequestMapping(value = "/cleanOperLog", method = RequestMethod.DELETE)
    public void cleanOperLog()
    {
        operLogMapper.cleanOperLog();
    }
}
