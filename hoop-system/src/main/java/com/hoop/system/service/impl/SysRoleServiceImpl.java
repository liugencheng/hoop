package com.hoop.system.service.impl;

import com.hoop.common.annotation.DataScope;
import com.hoop.common.constant.UserConstants;
import com.hoop.common.core.domain.entity.SysRole;
import com.hoop.common.core.domain.entity.SysUser;
import com.hoop.common.exception.ServiceException;
import com.hoop.common.utils.SecurityUtils;
import com.hoop.common.utils.StringUtils;
import com.hoop.common.utils.spring.SpringUtils;
import com.hoop.domain.SysRoleDept;
import com.hoop.domain.SysRoleMenu;
import com.hoop.domain.SysUserRole;
import com.hoop.system.mapper.SysRoleDeptMapper;
import com.hoop.system.mapper.SysRoleMapper;
import com.hoop.system.mapper.SysRoleMenuMapper;
import com.hoop.system.mapper.SysUserRoleMapper;
import com.hoop.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 角色 业务层处理
 *
 * @author dongua
 */
@RestController
public class SysRoleServiceImpl implements ISysRoleService {
    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SysRoleMenuMapper roleMenuMapper;

    @Resource
    private SysUserRoleMapper userRoleMapper;

    @Resource
    private SysRoleDeptMapper roleDeptMapper;

    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    @RequestMapping(value = "/selectRoleList", method = RequestMethod.GET)
    public List<SysRole> selectRoleList(@RequestBody SysRole role) {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    @Override
    @RequestMapping(value = "/selectRolesByUserId", method = RequestMethod.GET)
    public List<SysRole> selectRolesByUserId(@RequestParam("userId") Long userId) {
        List<SysRole> userRoles = roleMapper.selectRolePermissionByUserId(userId);
        List<SysRole> roles = selectRoleAll();
        for (SysRole role : roles) {
            for (SysRole userRole : userRoles) {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue()) {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    @RequestMapping(value = "/selectRolePermissionByUserId", method = RequestMethod.GET)
    public Set<String> selectRolePermissionByUserId(@RequestParam("userId") Long userId) {
        List<SysRole> perms = roleMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    @Override
    @RequestMapping(value = "/selectRoleAll", method = RequestMethod.GET)
    public List<SysRole> selectRoleAll() {
        // 需要使用到代理域的 this 代理对象调用此方法（全局处理：@EnableAspectJAutoProxy(exposeProxy = true)）
        // 之所以这样设计,而不是在数据库层面上查询所有的数据，
        // 是因为 selectRoleList() 是过滤数据之后的条件查询，
        // 也就是说，我们需要使用到数据过滤，也就是 @DataScope(deptAlias = "d")来过滤角色对应的数据
        // 不希望在此处再加 @DataScope(deptAlias = "d") 这一注解去实现过滤是因为这种方式会使 service 层和 mapper 层产生多余的冗余
        return SpringUtils.getAopProxy(this).selectRoleList(new SysRole());
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    @Override
    @RequestMapping(value = "/selectRoleListByUserId", method = RequestMethod.GET)
    public List<Long> selectRoleListByUserId(@RequestParam("userId") Long userId) {
        return roleMapper.selectRoleListByUserId(userId);
    }

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    @RequestMapping(value = "/selectRoleById", method = RequestMethod.GET)
    public SysRole selectRoleById(@RequestParam("roleId") Long roleId) {
        return roleMapper.selectRoleById(roleId);
    }

    /**
     * 校验角色名称是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/checkRoleNameUnique", method = RequestMethod.GET)
    public String checkRoleNameUnique(@RequestBody SysRole role) {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = roleMapper.checkRoleNameUnique(role.getRoleName());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/checkRoleKeyUnique", method = RequestMethod.GET)
    public String checkRoleKeyUnique(@RequestBody SysRole role) {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = roleMapper.checkRoleKeyUnique(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    @Override
    @RequestMapping(value = "/checkRoleAllowed", method = RequestMethod.GET)
    public void checkRoleAllowed(@RequestBody SysRole role) {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin()) {
            throw new ServiceException("不允许操作超级管理员角色");
        }
    }

    /**
     * 校验角色是否有数据权限
     *
     * @param roleId 角色id
     */
    @Override
    @RequestMapping(value = "/checkRoleDataScope", method = RequestMethod.GET)
    public void checkRoleDataScope(@RequestParam("roleId") Long roleId) {
        if (!SysUser.isAdmin(SecurityUtils.getUserId())) {
            SysRole role = new SysRole();
            role.setRoleId(roleId);
            List<SysRole> roles = SpringUtils.getAopProxy(this).selectRoleList(role);
            if (StringUtils.isEmpty(roles)) {
                throw new ServiceException("没有权限访问角色数据！");
            }
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/countUserRoleByRoleId", method = RequestMethod.GET)
    public int countUserRoleByRoleId(@RequestParam("roleId") Long roleId) {
        return userRoleMapper.countUserRoleByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/insertRole", method = RequestMethod.POST)
    public int insertRole(@RequestBody SysRole role) {
        // 新增角色信息
        roleMapper.insertRole(role);
        return insertRoleMenu(role);
    }

    /**
     * 修改保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/updateRole", method = RequestMethod.PUT)
    public int updateRole(@RequestBody SysRole role) {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与菜单关联
        roleMenuMapper.deleteRoleMenuByRoleId(role.getRoleId());
        return insertRoleMenu(role);
    }

    /**
     * 修改角色状态
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/updateRoleStatus", method = RequestMethod.PUT)
    public int updateRoleStatus(@RequestBody SysRole role) {
        return roleMapper.updateRole(role);
    }

    /**
     * 修改数据权限信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/authDataScope", method = RequestMethod.PUT)
    public int authDataScope(@RequestBody SysRole role) {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role) {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : role.getMenuIds()) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0) {
            rows = roleMenuMapper.batchRoleMenu(list);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(SysRole role) {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds()) {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0) {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/deleteRoleById", method = RequestMethod.DELETE)
    public int deleteRoleById(@RequestParam("roleId") Long roleId) {
        // 删除角色与菜单关联
        roleMenuMapper.deleteRoleMenuByRoleId(roleId);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(roleId);
        return roleMapper.deleteRoleById(roleId);
    }

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/deleteRoleByIds", method = RequestMethod.DELETE)
    public int deleteRoleByIds(@RequestParam("roleIds") Long[] roleIds) {
        for (Long roleId : roleIds) {
            checkRoleAllowed(new SysRole(roleId));
            SysRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new ServiceException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuMapper.deleteRoleMenu(roleIds);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDept(roleIds);
        return roleMapper.deleteRoleByIds(roleIds);
    }

    /**
     * 取消授权用户角色
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteAuthUser", method = RequestMethod.DELETE)
    public int deleteAuthUser(@RequestBody SysUserRole userRole) {
        return userRoleMapper.deleteUserRoleInfo(userRole);
    }

    /**
     * 批量取消授权用户角色
     *
     * @param roleId  角色ID
     * @param userIds 需要取消授权的用户数据ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteAuthUsers", method = RequestMethod.DELETE)
    public int deleteAuthUsers(@RequestParam("roleId") Long roleId, @RequestParam("userIds") Long[] userIds) {
        return userRoleMapper.deleteUserRoleInfos(roleId, userIds);
    }

    /**
     * 批量选择授权用户角色
     *
     * @param roleId  角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/insertAuthUsers", method = RequestMethod.POST)
    public int insertAuthUsers(@RequestParam("roleId") Long roleId, @RequestParam("userIds") Long[] userIds) {
        // 新增用户与角色管理
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        for (Long userId : userIds) {
            SysUserRole ur = new SysUserRole();
            ur.setUserId(userId);
            ur.setRoleId(roleId);
            list.add(ur);
        }
        return userRoleMapper.batchUserRole(list);
    }
}
