package com.hoop.system.service.impl;

import com.hoop.common.constant.UserConstants;
import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.common.core.domain.entity.SysDictType;
import com.hoop.common.exception.ServiceException;
import com.hoop.common.utils.DictUtils;
import com.hoop.common.utils.StringUtils;
import com.hoop.system.mapper.SysDictDataMapper;
import com.hoop.system.mapper.SysDictTypeMapper;
import com.hoop.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author dongua
 */
@RestController
public class SysDictTypeServiceImpl implements ISysDictTypeService
{
    @Resource
    private SysDictTypeMapper dictTypeMapper;

    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void init()
    {
        loadingDictCache();
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    @Override
    @RequestMapping(value = "/selectDictTypeList", method = RequestMethod.GET)
    public List<SysDictType> selectDictTypeList(@RequestBody SysDictType dictType)
    {
        return dictTypeMapper.selectDictTypeList(dictType);
    }

    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    @Override
    @RequestMapping(value = "/selectDictTypeAll", method = RequestMethod.GET)
    public List<SysDictType> selectDictTypeAll()
    {
        return dictTypeMapper.selectDictTypeAll();
    }

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    @Override
    @RequestMapping(value = "/selectDictDataByType", method = RequestMethod.GET)
    public List<SysDictData> selectDictDataByType(@RequestParam("dictType") String dictType)
    {
        List<SysDictData> dictDatas = DictUtils.getDictCache(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            return dictDatas;
        }
        dictDatas = dictDataMapper.selectDictDataByType(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            DictUtils.setDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    @Override
    @RequestMapping(value = "/selectDictTypeById", method = RequestMethod.GET)
    public SysDictType selectDictTypeById(@RequestParam("dictId") Long dictId)
    {
        return dictTypeMapper.selectDictTypeById(dictId);
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    @Override
    @RequestMapping(value = "/selectDictTypeByType", method = RequestMethod.GET)
    public SysDictType selectDictTypeByType(@RequestParam("dictType") String dictType)
    {
        return dictTypeMapper.selectDictTypeByType(dictType);
    }

    /**
     * 批量删除字典类型信息
     *
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteDictTypeByIds", method = RequestMethod.DELETE)
    public void deleteDictTypeByIds(@RequestParam("dictIds") Long[] dictIds)
    {
        for (Long dictId : dictIds)
        {
            SysDictType dictType = selectDictTypeById(dictId);
            if (dictDataMapper.countDictDataByType(dictType.getDictType()) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
            }
            dictTypeMapper.deleteDictTypeById(dictId);
            DictUtils.removeDictCache(dictType.getDictType());
        }
    }

    /**
     * 加载字典缓存数据
     */
    @Override
    @RequestMapping(value = "/loadingDictCache", method = RequestMethod.GET)
    public void loadingDictCache()
    {
        List<SysDictType> dictTypeList = dictTypeMapper.selectDictTypeAll();
        for (SysDictType dictType : dictTypeList)
        {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(dictType.getDictType());
            DictUtils.setDictCache(dictType.getDictType(), dictDatas);
        }
    }

    /**
     * 清空字典缓存数据
     */
    @Override
    @RequestMapping(value = "/clearDictCache", method = RequestMethod.GET)
    public void clearDictCache()
    {
        DictUtils.clearDictCache();
    }

    /**
     * 重置字典缓存数据
     */
    @Override
    @RequestMapping(value = "/resetDictCache", method = RequestMethod.GET)
    public void resetDictCache()
    {
        clearDictCache();
        loadingDictCache();
    }

    /**
     * 新增保存字典类型信息
     *
     * @param dict 字典类型信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/insertDictType", method = RequestMethod.POST)
    public int insertDictType(@RequestBody SysDictType dictType)
    {
        int row = dictTypeMapper.insertDictType(dictType);
        if (row > 0)
        {
            DictUtils.setDictCache(dictType.getDictType(), null);
        }
        return row;
    }

    /**
     * 修改保存字典类型信息
     *
     * @param dict 字典类型信息
     * @return 结果
     */
    @Override
    @Transactional
    @RequestMapping(value = "/updateDictType", method = RequestMethod.PUT)
    public int updateDictType(@RequestBody SysDictType dictType)
    {
        SysDictType oldDict = dictTypeMapper.selectDictTypeById(dictType.getDictId());
        dictDataMapper.updateDictDataType(oldDict.getDictType(), dictType.getDictType());
        int row = dictTypeMapper.updateDictType(dictType);
        if (row > 0)
        {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(dictType.getDictType());
            DictUtils.setDictCache(dictType.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 校验字典类型称是否唯一
     *
     * @param dict 字典类型
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/checkDictTypeUnique", method = RequestMethod.GET)
    public String checkDictTypeUnique(@RequestBody SysDictType dictType)
    {
        Long dictId = StringUtils.isNull(dictType.getDictId()) ? -1L : dictType.getDictId();
        SysDictType dictType0 = dictTypeMapper.checkDictTypeUnique(dictType.getDictType());
        if (StringUtils.isNotNull(dictType0) && dictType0.getDictId().longValue() != dictId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
