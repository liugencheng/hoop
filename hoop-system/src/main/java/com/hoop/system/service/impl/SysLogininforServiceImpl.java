package com.hoop.system.service.impl;

import com.hoop.domain.SysLogininfor;
import com.hoop.system.mapper.SysLogininforMapper;
import com.hoop.system.service.ISysLogininforService;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统访问日志情况信息 服务层处理
 *
 * @author dongua
 */
@RestController
public class SysLogininforServiceImpl implements ISysLogininforService {

    @Resource
    private SysLogininforMapper logininforMapper;
    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * 新增系统登录日志
     *
     * @param logininfor 访问日志对象
     */
    @Override
    @RequestMapping(value = "/insertLogininfor", method = RequestMethod.POST)
    public void insertLogininfor(@RequestBody SysLogininfor logininfor) {
        mongoTemplate.insert(logininfor);
//        logininforMapper.insertLogininfor(logininfor);
    }

    /**
     * 查询系统登录日志集合
     *
     * @param
     * @return 登录记录集合
     */
    @Override
    @RequestMapping(value = "/selectLogininforList", method = RequestMethod.GET)
    public List<SysLogininfor> selectLogininforList(@RequestBody SysLogininfor logininfor) {
//        SysLogininfor logininfor
//        return logininforMapper.selectLogininforList(logininfor);
        return mongoTemplate.findAll(SysLogininfor.class);
    }

    /**
     * 批量删除系统登录日志
     *
     * @param infoIds 需要删除的登录日志ID
     * @return
     */
    @Override
    @RequestMapping(value = "/deleteLogininforByIds", method = RequestMethod.DELETE)
    public int deleteLogininforByIds(@RequestParam("infoIds") Long[] infoIds) {
//        return logininforMapper.deleteLogininforByIds(infoIds);
        long count = 0L;
        for (long id : infoIds) {
            Query query = new Query(Criteria.where("infor_id").is(infoIds));
            DeleteResult deleteResult = mongoTemplate.remove(query, SysLogininfor.class);
            count = count + deleteResult.getDeletedCount();
        }

        return (int) count;
    }

    /**
     * 清空系统登录日志
     */
    @Override
    @RequestMapping(value = "/cleanLogininfor", method = RequestMethod.DELETE)
    public void cleanLogininfor() {
//        logininforMapper.cleanLogininfor();
        mongoTemplate.dropCollection(SysLogininfor.class);
    }
}
