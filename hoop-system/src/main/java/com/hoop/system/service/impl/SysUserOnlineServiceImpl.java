package com.hoop.system.service.impl;

import com.hoop.common.core.domain.model.LoginUser;
import com.hoop.common.utils.StringUtils;
import com.hoop.domain.SysUserOnline;
import com.hoop.system.service.ISysUserOnlineService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * 在线用户 服务层处理
 *
 * @author dongua
 */
@RestController
public class SysUserOnlineServiceImpl implements ISysUserOnlineService {
    /**
     * 通过登录地址查询信息
     *
     * @param ipaddr 登录地址
     * @param user   用户信息
     * @return 在线用户信息
     */
    @Override
    @RequestMapping(value = "/selectOnlineByIpaddr", method = RequestMethod.GET)
    public SysUserOnline selectOnlineByIpaddr(@RequestParam("ipaddr") String ipaddr, @RequestBody LoginUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过用户名称查询信息
     *
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    @RequestMapping(value = "/selectOnlineByUserName", method = RequestMethod.GET)
    public SysUserOnline selectOnlineByUserName(@RequestParam("userName") String userName,@RequestBody LoginUser user) {
        if (StringUtils.equals(userName, user.getUsername())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 通过登录地址/用户名称查询信息
     *
     * @param ipaddr   登录地址
     * @param userName 用户名称
     * @param user     用户信息
     * @return 在线用户信息
     */
    @Override
    @RequestMapping(value = "/selectOnlineByInfo", method = RequestMethod.GET)

    public SysUserOnline selectOnlineByInfo(@RequestParam("ipaddr") String ipaddr,@RequestParam("userName") String userName,@RequestBody LoginUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())) {
            return loginUserToUserOnline(user);
        }
        return null;
    }

    /**
     * 设置在线用户信息
     *
     * @param user 用户信息
     * @return 在线用户
     */
    @Override
    @RequestMapping(value = "/loginUserToUserOnline", method = RequestMethod.POST)
    public SysUserOnline loginUserToUserOnline(@RequestBody LoginUser user) {
        if (StringUtils.isNull(user) || StringUtils.isNull(user.getUser())) {
            return null;
        }
        SysUserOnline sysUserOnline = new SysUserOnline();
        sysUserOnline.setTokenId(user.getToken());
        sysUserOnline.setUserName(user.getUsername());
        sysUserOnline.setIpaddr(user.getIpaddr());
        sysUserOnline.setLoginLocation(user.getLoginLocation());
        sysUserOnline.setBrowser(user.getBrowser());
        sysUserOnline.setOs(user.getOs());
        sysUserOnline.setLoginTime(user.getLoginTime());
        if (StringUtils.isNotNull(user.getUser().getDept())) {
            sysUserOnline.setDeptName(user.getUser().getDept().getDeptName());
        }
        return sysUserOnline;
    }
}
