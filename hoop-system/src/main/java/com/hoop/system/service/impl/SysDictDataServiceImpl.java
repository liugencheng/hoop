package com.hoop.system.service.impl;

import com.hoop.common.core.domain.entity.SysDictData;
import com.hoop.common.utils.DictUtils;
import com.hoop.system.mapper.SysDictDataMapper;
import com.hoop.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author dongua
 */
@RestController
public class SysDictDataServiceImpl implements ISysDictDataService {
    @Resource
    private SysDictDataMapper dictDataMapper;

    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    @RequestMapping(value = "/selectDictDataList", method = RequestMethod.GET)
    public List<SysDictData> selectDictDataList(@RequestBody SysDictData dictData) {
        return dictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    @RequestMapping(value = "/selectDictLabel", method = RequestMethod.GET)
    public String selectDictLabel(@RequestParam("dictType") String dictType, @RequestParam("dictValue") String dictValue) {
        return dictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    @RequestMapping(value = "/selectDictDataById", method = RequestMethod.GET)
    public SysDictData selectDictDataById(@RequestParam("dictCode") Long dictCode) {
        return dictDataMapper.selectDictDataById(dictCode);
    }

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deleteDictDataByIds", method = RequestMethod.DELETE)
    public void deleteDictDataByIds(@RequestParam("dictCodes") Long[] dictCodes) {
        for (Long dictCode : dictCodes) {
            SysDictData data = selectDictDataById(dictCode);
            dictDataMapper.deleteDictDataById(dictCode);
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/insertDictData", method = RequestMethod.POST)
    public int insertDictData(@RequestBody SysDictData dictData) {
        int row = dictDataMapper.insertDictData(dictData);
        if (row > 0) {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(dictData.getDictType());
            DictUtils.setDictCache(dictData.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/updateDictData", method = RequestMethod.PUT)
    public int updateDictData(@RequestBody SysDictData dictData) {
        int row = dictDataMapper.updateDictData(dictData);
        if (row > 0) {
            List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(dictData.getDictType());
            DictUtils.setDictCache(dictData.getDictType(), dictDatas);
        }
        return row;
    }
}
