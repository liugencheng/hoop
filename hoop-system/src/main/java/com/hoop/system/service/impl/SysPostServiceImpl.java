package com.hoop.system.service.impl;

import com.hoop.common.constant.UserConstants;
import com.hoop.common.exception.ServiceException;
import com.hoop.common.utils.StringUtils;
import com.hoop.domain.SysPost;
import com.hoop.system.mapper.SysPostMapper;
import com.hoop.system.mapper.SysUserPostMapper;
import com.hoop.system.service.ISysPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 岗位信息 服务层处理
 *
 * @author dongua
 */
@RestController
public class SysPostServiceImpl implements ISysPostService
{
    @Resource
    private SysPostMapper postMapper;

    @Resource
    private SysUserPostMapper userPostMapper;

    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位信息集合
     */
    @Override
    @RequestMapping(value = "/selectPostList", method = RequestMethod.GET)
    public List<SysPost> selectPostList(@RequestBody SysPost post)
    {
        return postMapper.selectPostList(post);
    }

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    @Override
    @RequestMapping(value = "/selectPostAll", method = RequestMethod.GET)
    public List<SysPost> selectPostAll()
    {
        return postMapper.selectPostAll();
    }

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    @RequestMapping(value = "/selectPostById", method = RequestMethod.GET)
    public SysPost selectPostById(@RequestParam("postId") Long postId)
    {
        return postMapper.selectPostById(postId);
    }

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    @Override
    @RequestMapping(value = "/selectPostListByUserId", method = RequestMethod.GET)
    public List<Long> selectPostListByUserId(@RequestParam("postId") Long userId)
    {
        return postMapper.selectPostListByUserId(userId);
    }

    /**
     * 校验岗位名称是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/checkPostNameUnique", method = RequestMethod.GET)
    public String checkPostNameUnique(@RequestBody SysPost post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = postMapper.checkPostNameUnique(post.getPostName());
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/checkPostCodeUnique", method = RequestMethod.GET)
    public String checkPostCodeUnique(@RequestBody SysPost post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = postMapper.checkPostCodeUnique(post.getPostCode());
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/countUserPostById", method = RequestMethod.GET)
    public int countUserPostById(@RequestParam("postId") Long postId)
    {
        return userPostMapper.countUserPostById(postId);
    }

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/deletePostById", method = RequestMethod.DELETE)
    public int deletePostById(@RequestParam("postId") Long postId)
    {
        return postMapper.deletePostById(postId);
    }

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     * @throws Exception 异常
     */
    @Override
    @RequestMapping(value = "/deletePostByIds", method = RequestMethod.DELETE)
    public int deletePostByIds(@RequestParam("postIds") Long[] postIds)
    {
        for (Long postId : postIds)
        {
            SysPost post = selectPostById(postId);
            if (countUserPostById(postId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
        return postMapper.deletePostByIds(postIds);
    }

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/insertPost", method = RequestMethod.POST)
    public int insertPost(@RequestBody SysPost post)
    {
        return postMapper.insertPost(post);
    }

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    @RequestMapping(value = "/updatePost", method = RequestMethod.PUT)
    public int updatePost(@RequestBody SysPost post)
    {
        return postMapper.updatePost(post);
    }
}
