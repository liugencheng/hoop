package com.hoop.common.utils;

/**
 * 处理并记录日志文件
 *
 * @author dongua
 */
public class LogUtils {
    /**
     * 格式转换，空="",msg=[msg]
     *
     * @param msg 日志信息
     * @return 格式转换后的 msg
     */
    public static String getBlock(Object msg) {
        if (msg == null) {
            msg = "";
        }
        return "[" + msg + "]";
    }
}
