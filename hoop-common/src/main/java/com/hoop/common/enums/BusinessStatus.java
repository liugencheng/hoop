package com.hoop.common.enums;

/**
 * 操作状态
 * 
 * @author dongua
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
